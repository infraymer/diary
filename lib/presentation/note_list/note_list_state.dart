import 'package:diary/view/note_list/model/note_list_item_data.dart';

class NoteListState {
  NoteListState();
  factory NoteListState.data(List<NoteListItemData> data) = DataNoteListState;
  factory NoteListState.loading() = LoadingNoteListState;
}

class IdleNoteListState extends NoteListState {}
class LoadingNoteListState extends NoteListState {}
class DataNoteListState extends NoteListState {
  DataNoteListState(this.data);
  final List<NoteListItemData> data;
}
