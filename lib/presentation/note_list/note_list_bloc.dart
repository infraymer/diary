import 'dart:async';

import 'package:diary/main.dart';
import 'package:diary/view/note_list/mapper/note_list_item_mapper.dart';

import 'note_list_state.dart';

class NoteListBloc {
  final _state = StreamController<NoteListState>();

  Stream<NoteListState> get state => _state.stream;

  void fetchNotes() {
    noteCacheDatasource.getNotes().then((notes) {
      var list = notes.map(NoteListItemMapper.mapToNoteListItem).toList();
      _state.sink.add(NoteListState.data(list));
    });
  }

  void dispose() {
    _state.close();
  }
}
