import 'dart:async';

import 'package:diary/domain/note/model/note.dart';
import 'package:diary/main.dart';
import 'package:diary/presentation/note_create/note_create_state.dart';

class NoteCreateBloc {
  final _state = StreamController<NoteCreateState>();

  Stream<NoteCreateState> get state => _state.stream;

  Future<void> onDoneClicked(String content, int rating) async {
    var note = NoteData(
      time: DateTime.now().millisecondsSinceEpoch,
      content: content,
      rating: rating,
    );
    await noteCacheDatasource.addNote(note);
  }

  void dispose() {
    _state.close();
  }
}
