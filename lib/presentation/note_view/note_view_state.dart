import 'package:diary/domain/note/model/note.dart';

class NoteViewState {
  NoteViewState();
  factory NoteViewState.idle() = IdleNoteViewState;
  factory NoteViewState.data(NoteData note) = DataNoteViewState;
}

class IdleNoteViewState extends NoteViewState {}
class DataNoteViewState extends NoteViewState {
  final NoteData note;
  DataNoteViewState(this.note);
}