import 'dart:async';

import 'package:diary/main.dart';

import 'note_view_state.dart';

class NoteViewBloc {
  final _state = StreamController<NoteViewState>();

  Stream<NoteViewState> get state => _state.stream;

  void fetchNote(int time) {
    noteCacheDatasource.getNote(time).then((note) {
      _state.sink.add(DataNoteViewState(note));
    });
  }

  void dispose() {
    _state.close();
  }
}
