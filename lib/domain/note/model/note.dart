class NoteData {
  int time;
  String content;
  int rating = 4; // from 0 to 4

  NoteData({this.time, this.content, this.rating});

  Map<String, dynamic> toMap() => {
        'time': time,
        'content': content,
        'rating': rating,
      };

  static NoteData fromMap(Map<String, dynamic> map) => NoteData(
        time: map['time'],
        content: map['content'],
        rating: map['rating'],
      );
}
