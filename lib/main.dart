import 'package:diary/cache/note_cache_data_source.dart';
import 'package:diary/view/base/screens.dart';
import 'package:diary/view/note_create/ui/note_create_screen.dart';
import 'package:diary/view/note_list/ui/note_list_screen.dart';
import 'package:diary/view/note_view/note_view_screen.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

NoteCacheDataSource noteCacheDatasource;

void main() async {
  var dir = await getApplicationDocumentsDirectory();
  await dir.create(recursive: true);
  var dbPath = join(dir.path, 'diary.db');
  DatabaseFactory dbFactory = databaseFactoryIo;

  Database db = await dbFactory.openDatabase(dbPath);

  var store = intMapStoreFactory.store('note_store');
  noteCacheDatasource = NoteCacheDataSource(db, store);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      onGenerateRoute: generateRoute,
      initialRoute: Screen.NoteListScreen,
    );
  }
}

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case Screen.NoteListScreen:
      return MaterialPageRoute(builder: (context) => NoteListScreen());
    case Screen.NoteCreateScreen:
      return MaterialPageRoute(builder: (context) => NoteCreateScreen());
    case Screen.NoteViewScreen:
      return MaterialPageRoute(
        builder: (context) => NoteViewScreen(time: settings.arguments),
      );
    default:
      return MaterialPageRoute(builder: (context) => NoteListScreen());
  }
}

void stub() {}
