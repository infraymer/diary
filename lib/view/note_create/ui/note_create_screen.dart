import 'package:diary/presentation/note_create/note_create_bloc.dart';
import 'package:diary/view/note_create/ui/widget/mood_widget.dart';
import 'package:flutter/material.dart';

class NoteCreateScreen extends StatefulWidget {
  NoteCreateScreen({Key key}) : super(key: key);

  @override
  _NoteCreateScreenState createState() => _NoteCreateScreenState();
}

class _NoteCreateScreenState extends State<NoteCreateScreen> {
  NoteCreateBloc _bloc;

  final contentController = TextEditingController();
  final ratingController = ValueNotifier<int>(4);

  @override
  void initState() {
    _bloc = NoteCreateBloc();
    super.initState();
  }

  @override
  void dispose() {
    ratingController.dispose();
    contentController.dispose();
    super.dispose();
  }

  void _onDoneClicked() {
    _bloc
        .onDoneClicked(
      contentController.text,
      ratingController.value,
    )
        .then((_) {
      Navigator.of(context).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Новая запись"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.done),
              onPressed: _onDoneClicked,
            )
          ],
        ),
        body: Container(
            padding: EdgeInsets.all(16),
            child: ListView(
              children: <Widget>[
                MoodWidget(
                  moodValueController: ratingController,
                ),
                TextField(
                  controller: contentController,
                  autofocus: true,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(border: InputBorder.none),
                ),
              ],
            )));
  }
}
