import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:diary/main.dart';

class MoodWidget extends StatefulWidget {
  final ValueNotifier<int> moodValueController;
  final bool isEdit;

  MoodWidget({Key key, this.moodValueController, this.isEdit}) : super(key: key);

  @override
  _MoodWidgetState createState() => _MoodWidgetState();
}

class _MoodWidgetState extends State<MoodWidget> {
  int get _iconSelected => widget.moodValueController.value;

  double _sizeIcon = 36;
  var _moodIconPaths = [
    'assets/icons/mood_1.svg',
    'assets/icons/mood_2.svg',
    'assets/icons/mood_3.svg',
    'assets/icons/mood_4.svg',
    'assets/icons/mood_5.svg',
  ];
  var _moodIconColors = [
    Colors.red,
    Colors.red.shade300,
    Colors.orange,
    Colors.green.shade300,
    Colors.green
  ];

  Color _colorMoodSelected(int position) {
    if (position == _iconSelected) return _moodIconColors[position];
    return Colors.grey;
  }

  _onMoodClicked(int rating) {
    widget.moodValueController.value = rating;
    setState(() {});
  }

  _updateValue() {
    setState(stub);
  }

  @override
  void initState() {
    widget.moodValueController.addListener(_updateValue);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: _moodIconPaths.map((path) {
        return Container(
            child: GestureDetector(
          child: SizedBox(
            width: _sizeIcon,
            height: _sizeIcon,
            child: SvgPicture.asset(
              path,
              color: _colorMoodSelected(_moodIconPaths.indexOf(path)),
            ),
          ),
          onTap: () => _onMoodClicked(_moodIconPaths.indexOf(path)),
        ));
      }).toList(),
    );
  }
}
