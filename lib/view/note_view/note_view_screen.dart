import 'package:diary/domain/note/model/note.dart';
import 'package:diary/presentation/note_view/note_view_bloc.dart';
import 'package:diary/presentation/note_view/note_view_state.dart';
import 'package:diary/view/note_create/ui/widget/mood_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NoteViewScreen extends StatefulWidget {
  final int time;

  NoteViewScreen({Key key, this.time}) : super(key: key);

  @override
  _NoteViewScreenState createState() => _NoteViewScreenState();
}

class _NoteViewScreenState extends State<NoteViewScreen> {
  NoteViewBloc _bloc;

  final contentController = TextEditingController();
  ValueNotifier<int> ratingController;

  @override
  void initState() {
    _bloc = NoteViewBloc();
    _bloc.fetchNote(widget.time);
    super.initState();
  }

  @override
  void dispose() {
    contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          DateFormat("dd-MM HH:mm")
              .format(DateTime.fromMillisecondsSinceEpoch(widget.time)),
        ),
      ),
      body: StreamBuilder<NoteViewState>(
        stream: _bloc.state,
        initialData: IdleNoteViewState(),
        builder: (context, snapshot) {
          if (snapshot.data is DataNoteViewState) {
            DataNoteViewState state = snapshot.data;
            return _buildData(state.note);
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget _buildData(NoteData note) {
    if (note.rating != null) {
      ratingController = ValueNotifier<int>(note.rating);
      ratingController.value = note.rating;
    }

    return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          note.rating != null
              ? MoodWidget(
                  moodValueController: ratingController,
                )
              : SizedBox(),
          Container(
            margin: EdgeInsets.only(top: 24),
            child: Text(note.content),
          ),
        ],
      ),
    );
  }
}
