class Screen {
  static const String NoteListScreen = '/';
  static const String NoteCreateScreen = 'note_create';
  static const String NoteViewScreen = 'note_view';
}