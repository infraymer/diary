import 'package:diary/domain/note/model/note.dart';
import 'package:diary/view/note_list/model/note_list_item_data.dart';

class NoteListItemMapper {
  static NoteListItemData mapToNoteListItem(NoteData note) => NoteListItemData(
        time: note.time,
        content: note.content,
      );
}
