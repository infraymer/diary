class NoteListItemData {
  int time;
  String content;

  NoteListItemData({this.time, this.content});
}
