import 'package:diary/presentation/note_list/note_list_bloc.dart';
import 'package:diary/presentation/note_list/note_list_state.dart';
import 'package:diary/view/base/screens.dart';
import 'package:diary/view/note_list/model/note_list_item_data.dart';
import 'package:diary/view/note_list/ui/widget/note_list_item_widget.dart';
import 'package:flutter/material.dart';

class NoteListScreen extends StatefulWidget {
  @override
  _NoteListScreenState createState() => _NoteListScreenState();
}

class _NoteListScreenState extends State<NoteListScreen> {
  NoteListBloc _bloc;

  void _onAddClicked() {
    Navigator.pushNamed(context, Screen.NoteCreateScreen);
  }

  void _onItemClicked(NoteListItemData note) {
    var time = note.time;
    Navigator.pushNamed(context, Screen.NoteViewScreen, arguments: time);
  }

  @override
  Widget build(BuildContext context) {
    _bloc = NoteListBloc();

    _bloc.fetchNotes();

    return Scaffold(
      appBar: AppBar(
        title: Text("Дневничок"),
      ),
      body: Container(
        child: StreamBuilder<NoteListState>(
          stream: _bloc.state,
          initialData: IdleNoteListState(),
          builder: (context, snapshot) {
            if (snapshot.data is LoadingNoteListState) {
              return _buildLoading();
            } else if (snapshot.data is DataNoteListState) {
              DataNoteListState state = snapshot.data;
              return _buildList(state.data);
            } else {
              return Container();
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _onAddClicked,
      ),
    );
  }

  Widget _buildLoading() => Center(
        child: CircularProgressIndicator(),
      );

  Widget _buildList(List<NoteListItemData> list) => ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: NoteListItemWidget(data: list[index]),
          onTap: () => _onItemClicked(list[index]),
        );
      });
}
