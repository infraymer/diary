import 'package:diary/view/note_list/model/note_list_item_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NoteListItemWidget extends StatelessWidget {
  final NoteListItemData data;

  NoteListItemWidget({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8),
      child: Card(
        elevation: 4,
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                DateFormat("dd-MM-yy HH:mm")
                    .format(DateTime.fromMillisecondsSinceEpoch(data.time)),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Text(
                  data.content,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w100,
                    fontSize: 18,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
