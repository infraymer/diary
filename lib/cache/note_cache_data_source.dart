import 'package:diary/domain/note/model/note.dart';
import 'package:sembast/sembast.dart';

class NoteCacheDataSource {
  Database _db;
  StoreRef _store;

  NoteCacheDataSource(this._db, this._store);

  Future<void> addNote(NoteData note) async {
    await _store.add(_db, note.toMap());
  }

  Future<NoteData> getNote(int time) async {
    final finder = Finder(filter: Filter.equals('time', time));
    final recordSnapshot = await _store.findFirst(_db, finder: finder);
    return NoteData.fromMap(recordSnapshot.value);
  }

  Future<List<NoteData>> getNotes() async {
    final finder = Finder(sortOrders: [
      SortOrder('time', false),
    ]);
    final recordSnapshots = await _store.find(_db, finder: finder);
    return recordSnapshots.map((snapshot) {
      return NoteData.fromMap(snapshot.value);
    }).toList();
  }
}
